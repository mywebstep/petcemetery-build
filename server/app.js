/**
 * Main application file
 */

'use strict';

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var express = require('express');
var helmet = require('helmet');
var mongoose = require('mongoose');
var config = require('./config/environment');


// Connect to database
mongoose.connect(config.mongo.uri, config.mongo.options);

// Populate DB with sample data
if(config.seedDB) { require('./config/seed'); }

if(config.seedDBProd) { require('./config/seed.prod'); }


// Setup server
var app = express();

// Enable reverse proxy support in Express. This causes the
// the "X-Forwarded-Proto" header field to be trusted so its
// value can be used to determine the protocol. See 
// http://expressjs.com/api#app-settings for more details.
if(config.https) {
    app.enable('trust proxy');
}

app.use(helmet());
app.use(require('prerender-node').set('prerenderToken', 'Sotvfh3faX18v1qWfxU7'));
app.use(function(req, res, next) {
  if(config.https && !req.secure) {
    return res.redirect(['https://', req.get('Host'), req.url].join(''));
  }
  next();
});



var server = require('http').createServer(app);
require('./config/express')(app);
require('./routes')(app);

// Start server
server.listen(config.port, config.ip, function () {
  console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
});

// Expose app
exports = module.exports = app;
