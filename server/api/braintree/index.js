'use strict';

var express = require('express');
var controller = require('./braintree.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/client_token', auth.isAuthenticated(), controller.token);
router.post('/payment', auth.isAuthenticated(), controller.payment);

module.exports = router;