'use strict';

var _ = require('lodash');

var braintree = require("braintree");
var paymentController = require('../payment/payment.controller');

var gateway = braintree.connect({
    environment:  braintree.Environment.Production,
    merchantId:   'zytwn7j64xx72xpt',
    publicKey:    'yctcvr6vy3y8b8c6',
    privateKey:   '9d26f8d2a5c3b94f69962037ae63e64a'
});


exports.token = function(req, res) {
  gateway.clientToken.generate({}, function (err, response) {
    res.send(response.clientToken);
  });
};

exports.payment = function(req, res) {
  var nonce = req.body.braintree.nonce;
    
    var saleRequest = {
      amount: req.body.amount,
      paymentMethodNonce: nonce,
      options: {
        submitForSettlement: true
      }
    };
    
    gateway.transaction.sale(saleRequest, function (err, result) {
        if(!err){
            if(req.body.action.valueOf()  === 'creation'){
              paymentController.receivePaymentAfterAnimalCreation(req.body.animalId, req.body.amount, function(credited, activated, message){
                  if(!activated){
                    console.log(message);   
                  }
                  if(credited){
                    return res.send(200);
                  }
                  if(!credited){
                    return handleError(res, message);  
                  }
              });  
            }
            if(req.body.action.valueOf()  === 'gift'){
                paymentController.receivePaymentAfterGiftCreation(req.body.animalId, req.body.giftId, req.body.amount, function(credited, activated, message){
                  if(!activated){
                    console.log(message);   
                  }
                  if(credited){
                    return res.send(200);
                  }
                  if(!credited){
                    return handleError(res, message);  
                  }
                });     
            }
        }
        if(err){
            return handleError(res, err); 
        }
    });
};



function handleError(res, err) {
  console.log('braintree - '+err);
  return res.send(500, err);
}