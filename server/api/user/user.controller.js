'use strict';

var _ = require('lodash');
var User = require('./user.model');
var UserVerify = require('./user-verify.model');
var passport = require('passport');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var sendgrid  = require('sendgrid')('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');
var key = require('mongo-key-escape');
var https = require('https');

var SECRET = "6Le8XwsTAAAAAJaQBgshEH75ijtzpjJ04TYrKQQ-";


var limitPerPage = 20;

var validationError = function(res, err) {
  return res.json(422, err);
};

exports.verifyUser = function(req, res) {
    var token = req.params.token;
    UserVerify.findOne({token: token}, function (err, doc){
        if (err) {return handleError(res, err);}
        else if (!doc) {return handleError(res, 'Token not found.');}
        else {
            User.findOne({_id: doc._userId}, function (err, user) {
                if (err) return handleError(res, err);
                else if(user){
                    user.active = true;
                    user.markModified('active');
                    user.save(function(err) {
                        if (err) {return validationError(res, err);}
                        else {res.send(200);}
                    });
                }
                else {
                     return handleError(res, "User not found in database.");         
                }
              });
        }
    });
};

/**
 * Get list of users
 * restriction: 'admin'
 */
exports.getPage = function(req, res) {
  var page = 0;
  if(req.params.page){
    page = Math.max(0,req.params.page);
  }
  else{
    page = 0;
  }
  User.find().select('name disable email provider role money')
  .limit(limitPerPage)
      .skip(limitPerPage*page)
      .sort('-name')
    .exec(function (err, users) {
        if(err) { return handleError(res, err); }
        else {return res.json(200, users);}
      });
};

exports.findByName = function(req, res) {
  var name = req.params.name;
  var page = 0;
  if(req.params.page){
    page = Math.max(0,req.params.page);
  }
  else{
    page = 0;
  }
  User.find({name:  new RegExp(name, 'i')})
      .limit(limitPerPage)
      .skip(limitPerPage*page)
      .sort('-name')
      .exec(function (err, users) {
        if(err) { return handleError(res, err); }
        else {return res.json(200, users);}
      });
};

exports.countItemsForName = function(req, res) {
    
  var name = key.escape(req.params.name);

  User.count({name:  new RegExp(name, 'i')}).exec(function (err, count) {
      //console.log("count for name: "+name+" "+count);
        if(err) { return handleError(res, err); }
        else {return res.json(count);}
  });
};

/**
 * Creates a new user
 */
exports.create = function (req, res) {
    verifyRecaptcha(req.body.captcha, function(success) {
        if (success) {
              User.count().and([{email:  key.escape(req.body.email)},{provider:  'local'}
                ]).exec(function (err, count) {
                  if(err) { return handleError(res, err); }
                  else if(count >0){
                      User.findOne().and([{email:  key.escape(req.body.email)},{provider:  'local'}
                        ]).exec(function (err, user) {
                          if(err) { return handleError(res, err); }
                          else {
                              if(!user.active){
                                  sendConfirmationMail(user, req.protocol, req.get('host'), function(sent) {
                                    if(!sent){
                                       console.log("problem during sending message..");
                                    }
                                  });
                              }
                              return res.send(422, 'User already exist!');
                          }
                      });
                  }
                  else {
                      var newUser = new User(req.body);
                      newUser.provider = 'local';
                      newUser.role = 'user';
                      newUser.active = false;
                      newUser.money = 0;
                      newUser.save(function(err, user) {
                        if (err) return validationError(res, err);
                        sendConfirmationMail(user, req.protocol, req.get('host'), function(sent) {
                              //console.log(sent);
                            if(sent){
                                return res.send(200);
                            }
                            else{
                                console.log("problem during sending message..");
                                res.send(500, "problem during sending message..");
                            }
                        });
                        //var token = jwt.sign({_id: user._id }, config.secrets.session, { expiresInMinutes: 60*5 });
                        //res.json({ token: token });
                      });

                  }
              });
        }
        else {
            res.send(409, "Invalid Captcha");
        }
        
        
    });

}; 

/**
 * Get a single user
 */
exports.show = function (req, res, next) {
  var userId = key.escape(req.params.id);

  User.findById(userId, function (err, user) {
    if (err) return next(err);
    else if (!user) return res.send(401);
    else {res.json(user.profile);}
  });
};

/**
 * Deletes a user
 * restriction: 'admin'
 */
exports.destroy = function(req, res) {
  User.findByIdAndRemove(key.escape(req.params.id), function(err, user) {
    if(err) return res.send(500, err);
    else {return res.send(204);}
  });
};

// Updates an existing gift in the DB.
exports.changeMoney = function(req, res) {
  var userId = key.escape(req.params.id);
  var money = req.body.money;
  User.findById(userId, function (err, user) {
    if (err) { return handleError(res, err); }
    else if(!user) { return res.send(404); }
    else {
       user.money = money;
       user.save(function(err) {
        if (err) {return validationError(res, err);}
        else {res.send(200);}
      });
    }
  });
};

/**
 * Deletes a user
 * restriction: 'admin'
 */
/*exports.changeState = function(req, res) {
   var userId = req.user._id;
    
   User.findOneAndUpdate(userId, {$set: { active: false }}).exec(function (err, user) {
       console.log(user.active);
       user.markModified("active");
        user.save();

        if (err) return validationError(res, err);
        res.send(200);
      });
    
  };*/

exports.changeState = function(req, res) {
   var userId = key.escape(req.params.id);
   User.findById(userId, function (err, user) {
      user.disable = !user.disable;
      user.markModified('disable');
      user.markModified('name');
      user.save(function(err) {
        if (err) {return validationError(res, err);}
        else {res.send(200);}
      });
  });
};

/**
 * Change a users password
 */
exports.changePassword = function(req, res, next) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  User.findById(userId, function (err, user) {
    if(user.authenticate(oldPass)) {
      user.password = newPass;
      user.save(function(err) {
        if (err) {return validationError(res, err);}
        else {res.send(200);}
      });
    } else {
      res.send(403);
    }
  });
};

/**
 * Get my info
 */
exports.me = function(req, res, next) {
  var userId = req.user._id;
  User.findOne({
    _id: userId
  }, '-salt -hashedPassword', function(err, user) { // don't ever give out the password or salt
    if (err) {return next(err);}
    else if(!user) {return res.json(401);}
    else {res.json(user);}
  });
};

/**
 * Authentication callback
 */
exports.authCallback = function(req, res, next) {
  res.redirect('/');
};


exports.count = function(req, res, next) {
  User.count().exec(function (err, count) {
        if(err) { return handleError(res, err); }
        else { return res.json(count);}
      });
};

exports.limitPerPage = function(req, res, next) {
    return res.json(limitPerPage);

};

exports.debit = function(userId, amount, callback) {
     User.findOne({
        _id: userId
      }, '-salt -hashedPassword', function(err, user) { 
        if (err) { 
            console.log('debit: '+err);
            return callback(false);
        }
        if (!user) { 
            console.log('debit: '+'User not found');
            return callback(false);
        }
         
        if(+Number(amount).toFixed(2) <= +Number(user.money).toFixed(2)){
            user.money = +Number(user.money).toFixed(2) - +Number(amount).toFixed(2);
            user.save(function(err) {
                if (err) {
                    console.log('debit: '+err);
                    return callback(false);
                }
                return callback(true);
            });
        }
         else {
            console.log('debit: '+'Money is insuffisant!');
            return callback(false);
         }
      });
};

exports.credit = function(userId, amount, callback) {
     User.findOne({
        _id: userId
      }, '-salt -hashedPassword', function(err, user) { 
        if (err) { 
            console.log('credit: '+err);
            return callback(false);
        }
        if (!user) { 
            console.log('credit: '+'User not found');
            return callback(false);
        }

        user.money  =  (+Number(user.money).toFixed(2)) + (+Number(amount).toFixed(2));
        user.save(function(err) {
            if (err) {
                console.log('credit: '+err);
                return callback(false);
            }
            //console.log(amount);
            //console.log(user.money);
            return callback(true);
        });
      });
};

function handleError(res, err) {
  console.log(err);
  return res.send(500, err);
}

function sendConfirmationMail(user, protocol, host, callback){
    var verificationToken = new UserVerify({_userId: user._id});
    verificationToken.createVerificationToken(function (err, token) {
        if (err) {
            return callback(false);
        }
        var message =   'Bonjour ' + user.name + ',<br><br>' + 
                        'Bienvenu sur le site animal-paradise !<br>' + 
                        'Avant de pouvoir vous connecter sur notre site, veuillez confirmer votre enregistrement en cliquant sur ce lien: ' +
                        '<a href=\'' + protocol + "://" + host + "/login?mailConfirmation=" + token + '\'>Confirmer mon email</a><br><br>'+
                        'L\'équipe Animal-Paradise,'+ '<br><br><br>' +
                        'Goedendag ' + user.name + ',<br><br>' + 
                        'Welkom op de website van animal-paradise !<br>' + 
                        'Vooraleer u zich kan aanmelden op onze site, gelieve uw inschrijving te bevestigen door op deze link te klikken: ' +
                        '<a href=\'' + protocol + "://" + host + "/login?mailConfirmation=" + token + '\'>Mijn e-mail bevestigen</a><br><br>'+
                        'Het Animal-Paradise-team'+ '<br><br><br>' +
                        'Hello ' + user.name + ',<br><br>' + 
                        'Welcome to the web site animal-paradise !<br>' + 
                        'Please confirm your registration before your first login: ' +
                        '<a href=\'' + protocol + "://" + host + "/login?mailConfirmation=" + token + '\'>Confirm my email</a><br><br>'+
                        'The team Animal-Paradise,';

        sendgrid.send({
            to:       user.email,
            from:     'do-not-reply@animals-paradise.eu',
            subject:  'Animal-Paradise: email confirmation - bevestiging van uw e-mail',
            html:     message
        }, function(err, json) {
            if (err) { 
                console.error(err); 
                return callback(false);
            }
            return callback(true);
        });
        return callback(true);
    });
    
}



function verifyRecaptcha(key, callback) {
        https.get("https://www.google.com/recaptcha/api/siteverify?secret=" + SECRET + "&response=" + key, function(res) {
                var data = "";
                res.on('data', function (chunk) {
                        data += chunk.toString();
                });
                res.on('end', function() {
                    try {
                            var parsedData = JSON.parse(data);
                            return callback(parsedData.success);
                    } catch (e) {
                            return callback(false);
                    }
                });
        });
}
