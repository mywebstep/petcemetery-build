'use strict';

var mongoose = require('mongoose'),
    validators = require('mongoose-validators');
var Schema = mongoose.Schema;
var uuid = require('node-uuid');

// Verification token model
var verificationTokenSchema = new Schema({
    _userId: {type: Schema.Types.ObjectId, required: true, ref: 'User'},
    token: {type: String, required: true},
    createdAt: {type: Date, required: true, default: Date.now, expires: '4h'}
});


verificationTokenSchema.methods.createVerificationToken = function (done) {
    var verificationToken = this;
    var token = uuid.v4();
    verificationToken.set('token', token);
    verificationToken.save( function (err) {
        if (err) return done(err);
        return done(null, token);
    });
};

module.exports = mongoose.model('VerificationToken', verificationTokenSchema);

