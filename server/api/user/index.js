'use strict';

var express = require('express');
var controller = require('./user.controller');
var config = require('../../config/environment');
var auth = require('../../auth/auth.service');

var router = express.Router();

//router.get('/', auth.hasRole('admin'), controller.index);
router.post('/verify/:token', controller.verifyUser);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);
router.get('/me', auth.isAuthenticated(), controller.me);
router.put('/state/:id', auth.hasRole('admin'), controller.changeState);
router.put('/:id/password', auth.isAuthenticated(), controller.changePassword);
router.put('/:id/money', auth.hasRole('admin'), controller.changeMoney);

router.post('/', controller.create);
router.get('/findbyname/:name/:page?', auth.hasRole('admin'), controller.findByName);
router.get('/countItems', auth.hasRole('admin'), controller.count);
router.get('/countItemsForName/:name', auth.hasRole('admin'), controller.countItemsForName);
router.get('/limitPerPage', auth.hasRole('admin'), controller.limitPerPage);
router.get('/getpage/:page', auth.hasRole('admin'), controller.getPage);

router.get('/:id', auth.isAuthenticated(), controller.show);



 

module.exports = router;
