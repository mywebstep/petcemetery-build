'use strict';

var mongoose = require('mongoose'),
    validators = require('mongoose-validators'),
    Schema = mongoose.Schema;

var alphanumMatcher = [ /^[a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸ _'’"«»-]+$/i, "The string should used special caracter" ];
var descriptionMatcher = [ /^[a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸ& _.(),;:!?"«»'’-]+$/i , "The string should used only alaphanumeric or punction" ];

function nameValidator (val) {
  return val.length>=3 && val.length<=50;
}

function raceValidator (val) {
  if(val && val.length !==0){
    return val.length>=2 && val.length<=50;
  }
  else {
      return true;//race is optional
  }
}

function descriptionValidator (val) {
  return val.length>=10 && val.length<=400;
}

function descriptionAuthorValidator (val) {
  return val.length>=3 && val.length<=50;
}

function messageGiftValidator (val) {
  return val.length>=3 && val.length<=100;
}



var AnimalSchema = new Schema({
  name: { type: String, required: true, validate: nameValidator, index: true },
  visits: { type: Number, required: true, min:0, default: 0},
  sex: { type: String, enum: ['M', 'F'], required: true },
  birthDate: { type: Date, required: true },
  deathDate: { type: Date, required: true },
  species: {type: Schema.Types.ObjectId, ref: 'Species' , required: true, index: true},
  race: { type: String, validate: raceValidator, index: true},
  photoUrl: { type: String, validate: validators.isURL({require_tld: false}), required: true},
  resources: [{url: { type: String, validate: validators.isURL({require_tld: false})}, active:  { type: Boolean, default: false , required: true}}],
  buyingResoures: { type: Number, required: true, min:0, default: 1},
  video: { type: String, validate: validators.isURL()},
  description: { type: String, required: true, validate: descriptionValidator},
  descriptionAuthor: { type: String, required: true, validate: descriptionAuthorValidator},
  creationDate: { type: Date, default: Date.now , required: true, index: true},
  active: { type: Boolean, default: true , required: true},
  creator: {type: Schema.Types.ObjectId, ref: 'User', required: true, index: true},
  gifts: [{gift: {type: Schema.Types.ObjectId, ref: 'Gift'}, message: {type: String, validate: messageGiftValidator, required: true}, date: { type: Date, default: Date.now},creator: {type: Schema.Types.ObjectId, ref: 'User'}, active:  { type: Boolean, default: false , required: true} }]
});

module.exports = mongoose.model('Animal', AnimalSchema);

AnimalSchema.path('birthDate').validate(function(v) {
    if (v > this.deathDate) {
        return false;
    } else {
        return true;
    }
}, 'Deathdate must be greater than BirthDate');
/*
AnimalSchema.pre('validate', function(next) {
    if (this.deathDate > this.birthDate) {
        next('Deathdate must be greater than BirthDate');
    } else {
        next();
    }
});*/
