'use strict';

var express = require('express');
var controller = require('./animal.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/limitPerPage', auth.hasRole('admin'), controller.limitPerPage);
router.get('/countItems', auth.hasRole('admin'), controller.count);
router.get('/findrecent/:page?', controller.findRecent);

router.get('/find/:name/:race/:lang/:page?', controller.findByNameAndRaceOrSpecies);
router.get('/findallrecent/:page?', auth.hasRole('admin'), controller.findAllRecent);

router.get('/findbyname/:name/:page?', controller.findByName);
router.get('/findallbyname/:name/:page?', auth.hasRole('admin'), controller.findAllByName);
router.get('/findbyrace/:race/:lang/:page?', controller.findByRace);
router.get('/:id', controller.show);

router.put('/state/:id', auth.hasRole('admin'), controller.changeState);
router.get('/countItemsForName/:name', auth.hasRole('admin'), controller.countItemsForName);
router.get('/getuserpages/:id', auth.isAuthenticated(), controller.getUserPages);
router.post('/', auth.isAuthenticated(), controller.create);
router.put('/:id', auth.isAuthenticated(), controller.update);
router.patch('/:id', auth.isAuthenticated(), controller.update);
router.put('/addgift/:id', auth.isAuthenticated(), controller.addGift);


module.exports = router;
