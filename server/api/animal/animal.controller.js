'use strict';

var _ = require('lodash');
var Animal = require('./animal.model');
var Species = require('../species/species.model');
var Gift = require('../gift/gift.model');
var userController = require('../user/user.controller');
var FB = require('fb');
var key = require('mongo-key-escape');

FB.setAccessToken('CAAL1qBGv7HUBAO27MxG5ITsXxAKr3wQH5r8gdz8BJFOILUHhHfoen5ZAEtwyTdcsZAHZB9bw1vZBf0EJ1vWn7oFub3BKnjFvFnXXdzEcDl3uZBHZBzAgkvTZBx8QEvz6wZAddqNqvt9ZAHZCYehqqlEZC8oZBKlF93o3pAd8zaIimEEfFnU2KtJOlEHi');
var limitPerPage = 20;



// Get list of animals
exports.index = function(req, res) {
  Animal.find(function (err, animals) {
    if(err) { return handleError(res, err); }
    else { return res.json(200, animals); }
  });
};

// Get a single animal
exports.show = function(req, res) {
  var id = key.escape(req.params.id);
  Animal.findByIdAndUpdate(id, {$inc: { visits: 1 }}).populate('creator', 'name').populate('species').populate('gifts.creator', 'name').populate('gifts.gift')
    .exec(function (err, animal) {
      if(err) { return handleError(res, err); }
      else if(!animal || !animal.active) { return res.send(404); }
      else {return res.json(animal);}
    });
};

exports.findByNameAndRaceOrSpecies = function(req, res) {
  var name = key.escape(req.params.name), race = key.escape(req.params.race);
  var lang = key.escape(req.params.lang);
  var page = Math.max(0,req.params.page);

  var species;
  var query = {};
  query[lang] = new RegExp(race, 'i');
  Species.find(query).select('id').exec(function (err, result) {
    if(err) {
      console.error(err);
      return handleError(res, err);
    }
    else {
        species = Object.keys(result).map(function(k) { return result[k] });
        Animal.find().populate('species')
          .and([
            {name:  new RegExp(name, 'i')},
            {active:  true},
            {$or: [{race:  new RegExp(race, 'i')}, {species:  {$in: species}}]}
          ])
          .limit(limitPerPage)
          .skip(limitPerPage*page)
          .sort('-name')
          .exec(function (err, animals) {
            if(err) { return handleError(res, err); }
            else {return res.json(200, animals);}
          });
    }
  });
};

exports.findRecent = function(req, res) {
  var page = Math.max(0,req.params.page);
  Animal.find({active:  true})
      .populate('species')
      .limit(limitPerPage)
      .skip(limitPerPage*page)
      .sort('-creationDate')
      .exec(function (err, animals) {
        if(err) {
          console.error(err);
          return handleError(res, err); }
        else { return res.json(200, animals);}
      });
};

exports.findAllRecent = function(req, res) {
  var page = Math.max(0,req.params.page);
  Animal.find()
      .populate('species')
      .limit(limitPerPage)
      .skip(limitPerPage*page)
      .sort('-creationDate')
      .exec(function (err, animals) {
        if(err) {
          console.error(err);
          return handleError(res, err); }
        else{
            return res.json(200, animals);
        }
      });
};

exports.findByName = function(req, res) {
  var name = key.escape(req.params.name);
  var page = Math.max(0,req.params.page);
  Animal.find()
      .populate('species')
      .and([
        {name:  new RegExp(name, 'i')},
        {active:  true}
      ])
      .limit(limitPerPage)
      .skip(limitPerPage*page)
      .sort('-name')
      .exec(function (err, animals) {
        if(err) { return handleError(res, err); }
        else{ 
            return res.json(200, animals);
        }
      });
};

exports.findAllByName = function(req, res) {
  var name = key.escape(req.params.name);
  var page = Math.max(0,req.params.page);
  Animal.find({name:  new RegExp(name, 'i')})
      .populate('species')
      .limit(limitPerPage)
      .skip(limitPerPage*page)
      .sort('-name')
      .exec(function (err, animals) {
        if(err) { return handleError(res, err); }
        else{
            return res.json(200, animals);
        }
      });
};

exports.findByRace = function(req, res) {
  var race = key.escape(req.params.race);
  var lang = key.escape(req.params.lang);
  var page = Math.max(0,req.params.page);
  var species;
  var query = {};
  query[lang] = new RegExp(race, 'i');
    Species.find(query).select('id').exec(function (err, result) {
      if(err) {
        console.error(err);
        return handleError(res, err);
      }
      else{
          species = Object.keys(result).map(function(k) { return result[k] });
          Animal.find().populate('species')
            .and([
                {active:  true},
                {$or: [{race:  new RegExp(race, 'i')}, {species:  {$in: species}}]}
            ])
            .limit(limitPerPage)
            .skip(limitPerPage*page)
            .sort('-name')
            .exec(function (err, animals) {
              if(err) { return handleError(res, err); }
              else {return res.json(200, animals);}
            });
      }
    });

};

// Creates a new animal in the DB.
exports.create = function(req, res) {
  if(req.body._id){
      return res.json(201, "Use put or patch method for update of exsting animal");
  }
    
  var newAnimal = new Animal(req.body);
  newAnimal.creator = req.user._id;
  newAnimal.buyingResoures = 1;
    
  updateResources(req.user, newAnimal, function(amount, resourcesActivated) {
    userController.debit(req.user._id, amount, function(debited) {
        if(debited) {
              newAnimal.save(function(err, animal) {
                if(err) {
                  userController.credit(req.user._id, amount, function(debited) {
                    console.log(err);
                    return handleError(res, err); 
                  });
                }
                else{
                    if(animal){
                        var link_page = "https://www.animal-paradise.eu/animal/" + animal._id;
                        FB.api('370742223120126/feed', 'post', { link: link_page}, function (res) {
                            if(!res || res.error) {
                                console.log(!res ? 'error occurred' : res.error);
                                return;
                            }
                        });
                        var obj = {
                                  resourcesActivated: resourcesActivated,
                                  animal: animal
                              };
                        return res.json(201, obj);
                    }
                    else{
                        return handleError(res, "Animal not saved"); 
                    }
                }
              });
        }
        else {
            console.log('not debited');
            return handleError(res, 'Transaction problème during debiting user amount.'); 
        }
    });   
  });
};

exports.resourcesActivation = function(user, animalId, callback) {
  Animal.findById(animalId, function (err, animal) {
      if(err) { return callback(false); }
      if(!animal) { return callback(false); }
      

      updateResources(user, animal, function(amount, resourcesActivated) {
        userController.debit(user._id, amount, function(debited) {
            if(debited) {
                  animal.save(function(err, animal) {
                    if(err) {
                        userController.credit(user._id, amount, function(debited) {
                           return callback(false); 
                        });
                    }
                    return callback(true);
                  });
            }
            else {
                return callback(false); 
            }
        }); 
          
      });
  }); 
};


exports.addGift = function(req, res) {   
    Gift.findById(key.escape(req.body.gift), function (err, gift) {
        if(err) { return handleError(res, err); }
        else if(!gift) { return res.send(404); }
        else {
            userController.debit(req.user._id, gift.price, function(debited) {
                if(debited){
                    req.body.active = true;
                }
                else {
                    req.body.active = false;
                }
                Animal.findByIdAndUpdate(key.escape(req.params.id), {$push: {gifts: req.body}}, { 'new': true}).populate('creator', 'name').populate('species').populate('gifts.creator', 'name').populate('gifts.gift').exec(function (err, animal) {
                  if(err) { return handleError(res, err); }
                  else if(!animal) { return res.send(404); }
                  else {
                    return res.json(animal.gifts[animal.gifts.length-1]._id);
                  }
                });
            });
        }       
    });
};

// Updates an existing animal in the DB.
exports.update = function(req, res) {

  //used for merge see thing.controller.js
  //if(req.body._id) { delete req.body._id; }
  Animal.findById(key.escape(req.params.id), function (err, animal) {
    if (err) { return handleError(res, err); }
    else if(!animal) { return res.send(404); }
    else{
      

        if(animal.creator && animal.creator.toString() === req.user._id.toString()) {// authorize update only by the creator
          animal.name = key.escape(req.body.name);
          animal.sex = req.body.sex;
          animal.birthDate = req.body.birthDate;
          animal.deathDate = req.body.deathDate;
          animal.species = req.body.species;
          animal.race = key.escape(req.body.race);
          animal.photoUrl = req.body.photoUrl;
          animal.resources = req.body.resources;
          animal.description = key.escape(req.body.description);
          animal.descriptionAuthor = key.escape(req.body.descriptionAuthor);
          animal.video = req.body.video;

          updateResources(req.user, animal, function(amount, resourcesActivated) {
              userController.debit(req.user._id, amount, function(debited) {
                if(debited) {
                      //var updated = _.merge(animal, req.body);
                      animal.save(function (err) {
                        if (err) {
                          userController.credit(req.user._id, amount, function(debited) {
                            console.log(err);
                            return handleError(res, err); 
                          });
                        }
                        else{
                            var obj = {
                              resourcesActivated: resourcesActivated,
                              animal: animal
                            };
                            return res.json(200, obj);
                        }
                      });
                }
                else {
                    console.log('not debited');
                    return handleError(res, 'Transaction problème during debiting user amount.'); 
                }
            });     
          });
       }
       else {
          return res.send(403); //not authorized
       }
    }
  });
};

exports.getUserPages = function(req, res){
    var userId = req.user._id;
     Animal.find()
     .and([
            {active:  true},
            {creator:  userId}
     ])
     .select('id name photoUrl sex visits')
      .sort('name')
      .exec(function (err, animals) {
        if(err) { return handleError(res, err); }
        else {return res.json(200, animals);}
    });
    
};

exports.changeState = function(req, res) {
   var animalId = key.escape(req.params.id);
   Animal.findById(animalId, function (err, animal) {
      animal.active = !animal.active;
      animal.save(function(err) {
        if (err) { return handleError(res, err); }
        else{ res.send(200);}
      });
    
  });
};

exports.countItemsForName = function(req, res) {
    
  var name = key.escape(req.params.name);

  Animal.count({name:  new RegExp(name, 'i')}).exec(function (err, count) {
        if(err) { return handleError(res, err); }
        else { return res.json(count);}
  });
};

exports.count = function(req, res, next) {
  Animal.count().exec(function (err, count) {
        if(err) { return handleError(res, err); }
        else { return res.json(count);}
      });
}

exports.limitPerPage = function(req, res, next) {
    return res.json(limitPerPage);
}

// Deletes a animal from the DB.
exports.destroy = function(req, res) {
  Animal.findById(req.params.id, function (err, animal) {
    if(err) { return handleError(res, err); }
    if(!animal) { return res.send(404); }
    animal.remove(function(err) {
      if(err) { return handleError(res, err); }
      else {return res.send(204);}
    });
  });
};

function handleError(res, err) {
  console.log(err);
  if(err){
    return res.send(500, err);
  }
  else{
    return res.send(500);
  }
}

function updateResources (user, animal, callback) {
    var amount = 0;
    var buyingResources = animal.buyingResoures ; 
    var activeResources = true;
    if(animal.resources){
        var arrayLength = animal.resources.length;
            for (var i = 0; i < arrayLength; i++) {
                if(buyingResources >0) {
                  buyingResources--;
                  animal.resources[i].active = true;
                }
                else {
                    if( ((+Number(amount).toFixed(2)) + (+Number(0).toFixed(2))) <= Number(user.money).toFixed(2)){
                        amount = (+Number(amount).toFixed(2)) + (+Number(0).toFixed(2));
                        animal.buyingResoures++;
                        animal.resources[i].active = true;
                    }
                    else {
                        activeResources = false;
                        animal.resources[i].active = false; 
                    }         
                }
            }
   }
   return callback(amount, activeResources);
}
