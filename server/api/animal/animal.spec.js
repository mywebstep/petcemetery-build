'use strict';

var should = require('should');
var app = require('../../app');
var request = require('supertest');

/*describe('GET /api/animals/:id', function() {

  it('should respond with one element : Dandy', function(done) {
    request(app)
      .get('/api/animals/550467e8bfd12eac154ad6af')
      .expect(200)
      .expect('Content-Type', text/plain)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.should.be.instanceof(Array);
        res.body.should.have.length(1);
        done();
      });
  });
});*/

describe('GET /api/animals', function() {

  /*it('should respond with JSON array', function(done) {
    request(app)
      .get('/api/animals')
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.should.be.instanceof(Array);
        done();
      });
  });*/
});

describe('GET /api/animals/find', function() {

  it('should respond with JSON array with one element when search for name=Dand and race=cocke ', function(done) {
    request(app)
      .get('/api/animals/find/Dand/cocKe/fr')
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.should.be.instanceof(Array);
        res.body.should.have.length(1);
        done();
      });
  });

  it('should respond with JSON array with one element when search for name=Dand and race=chien', function(done) {
    request(app)
      .get('/api/animals/find/Dand/chien/fr')
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.should.be.instanceof(Array);
        res.body.should.have.length(1);
        done();
      });
  });
});

describe('GET /api/animals/findbyname', function() {

  it('should respond with JSON array with one element when search for Dand', function(done) {
    request(app)
      .get('/api/animals/findbyname/Dand')
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.should.be.instanceof(Array);
        res.body.should.have.length(1);
        done();
      });
  });
});

describe('GET /api/animals/findbyrace', function() {

  it('should respond with JSON array with one element when search for CoCker', function(done) {
    request(app)
      .get('/api/animals/findbyrace/CoCker/fr')
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.should.be.instanceof(Array);
        res.body.should.have.length(1);
        done();
      });
  });

  it('should respond with JSON array with two elements when search for chien', function(done) {
    request(app)
      .get('/api/animals/findbyrace/chien/fr')
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.should.be.instanceof(Array);
        res.body.should.have.length(14);
        done();
      });
  });
});


describe('GET /api/animals/findrecent', function() {

  it('should respond with JSON array with 20 elements for first page', function(done) {
    request(app)
      .get('/api/animals/findrecent/0')
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.should.be.instanceof(Array);
        res.body.should.have.length(20);
        done();
      });
  });
  it('should respond with JSON array with 13 elements for second page', function(done) {
    request(app)
      .get('/api/animals/findrecent/1')
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) return done(err);
        res.body.should.be.instanceof(Array);
        res.body.should.have.length(13);
        done();
      });
    });
});
