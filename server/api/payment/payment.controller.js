'use strict';

var _ = require('lodash');
var Animal = require('../animal/animal.model');
var userController = require('../user/user.controller');
var animalController = require('../animal/animal.controller');
var Gift = require('../gift/gift.model');
var User = require('../user/user.model');


exports.receivePaymentAfterAnimalCreation = function(animalId, amount, callback) {
    Animal.findById(animalId, function (err, animal) { 
        if(err) return callback(false, false, 'PaymentAfterAnimalCreation: error during find animal'); 
        if(!animal) return callback(false, false, 'PaymentAfterAnimalCreation: Animal not found'); 
        if(animal){
            userController.credit(animal.creator, amount, function(credited) {
                if(credited){
                    User.findById(animal.creator, function (err, user) {
                        if (err) return callback(true, false, 'PaymentAfterAnimalCreation: Credited but not activated. Error during searching user.'); 
                        if (!user) return callback(true, false, 'PaymentAfterAnimalCreation: Credited but not activated. User not found.');
                        if(user){
                            animalController.resourcesActivation(user, animalId, function(activated) { 
                                if(activated){
                                    return callback(true, true, 'PaymentAfterAnimalCreation: Amount credited and resources activated.');
                                }
                                else{
                                    return callback(true, false, 'PaymentAfterAnimalCreation: Credited but not activated. Error during animal activation.');
                                }
                            });
                        }
                    });

                }
                else{
                    return callback(false, false, 'PaymentAfterAnimalCreation: Account not credited.'); 
                }

            });
        }
    });
};


exports.receivePaymentAfterGiftCreation = function(animalId, giftId, amount, callback) {
    Animal.findById(animalId, function (err, animal) { 
        if(err) return callback(false, false, 'PaymentAfterGiftCreation: error during find animal'); 
        if(!animal) return callback(false, false, 'PaymentAfterGiftCreation: Animal not found'); 
        if(animal){
            var pos = -1;
            for (var i = 0; i < animal.gifts.length ; i++) {
                if(animal.gifts[i]._id.toHexString() === giftId) {  
                    pos=i;
                }
            }
            if(pos !== -1) {
                userController.credit(animal.gifts[pos].creator, amount, function(credited) {
                    if(credited){
                        Gift.findById(animal.gifts[pos].gift, function (err, gift) {
                            if(err) return callback(true, false, 'PaymentAfterGiftCreation: Credited but not activated. Error during searching gift.'); 
                            if(!gift) return callback(true, false, 'PaymentAfterGiftCreation: Credited but not activated. Gift not found.');
                            if(gift) { 
                                userController.debit(animal.gifts[pos].creator, gift.price, function(debited) {
                                    if(debited){
                                        animal.gifts[pos].active = true;
                                        animal.save(function (err) {
                                            if (err) {
                                                userController.credit(animal.gifts[pos].creator, gift.price, function(debited) {
                                                   return callback(true, false, 'PaymentAfterGiftCreation: Credited but not activated. Saved animal error.');
                                                }); 
                                            }
                                            else {return callback(true, true, 'PaymentAfterGiftCreation: Amount credited and gift activated.');}
                                        });
                                    }
                                    else {return callback(true, false, 'PaymentAfterGiftCreation: Credited but not debited.');}
                                });   
                            }
                        });   
                    }
                    else {return callback(false, false, 'PaymentAfterGiftCreation: Account not credited.');}
                });
            }
            else {return callback(false, false, 'PaymentAfterGiftCreation: Account not credited. Gift not found');}
        }
    });
};

