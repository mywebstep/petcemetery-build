'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

function nameValidator (val) {
  return val.length>=3 && val.length<=50;
}
var alphanumMatcher = [ /^[a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸ _'’"«»-]+$/i, "The string should used special caracter" ];
var SpeciesSchema = new Schema({
  active: { type: Boolean, default: true , required: true },
  fr: { type: String, required: true, match: alphanumMatcher, validate: nameValidator, index: true },
  nl: { type: String, required: true, match: alphanumMatcher, validate: nameValidator, index: true },
  en: { type: String, required: true, match: alphanumMatcher, validate: nameValidator, index: true },
});

module.exports = mongoose.model('Species', SpeciesSchema);
