'use strict';

var _ = require('lodash');
var Species = require('./species.model');
var key = require('mongo-key-escape');

// Get list of speciess
/*exports.index = function(req, res) {
  Species.find(function (err, speciess) {
    if(err) { return handleError(res, err); }
    return res.json(200, speciess);
  });
};*/

// Get a single species
exports.show = function(req, res) {
  Species.findById(key.escape(req.params.id), function (err, species) {
    if(err) { return handleError(res, err); }
    else if(!species) { return res.send(404); }
    else {return res.json(species);}
  });
};

// Creates a new species in the DB.
exports.create = function(req, res) {
  Species.create(req.body, function(err, species) {
    if(err) { return handleError(res, err); }
    else {return res.json(201, species);}
  });
};

// Updates an existing species in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Species.findById(key.escape(req.params.id), function (err, species) {
    if (err) { return handleError(res, err); }
    else if(!species) { return res.send(404); }
    else {
        var updated = _.merge(species, req.body);
        updated.save(function (err) {
          if (err) { return handleError(res, err); }
          else {return res.json(200, species);}
        });
    }
  });
};

exports.changeState = function(req, res) {
   var speciesId = key.escape(req.params.id);
   Species.findById(speciesId, function (err, species) {
      species.active = !species.active;
      species.save(function(err) {
        if (err) { return handleError(res, err); }
        else {res.send(200);}
      });
    
  });
};

exports.findByName = function(req, res) {
  var name = key.escape(req.params.name);
  var lang = req.params.lang;
  if(lang === 'fr'){
    Species.find()
     .and([
        {fr:  new RegExp(name, 'i')},
        {active:  true}
      ])
      .sort('fr')
      .exec(function (err, animals) {
        if(err) { return handleError(res, err); }
        else {return res.json(200, animals);}
      });
  }
  else if(lang === 'nl'){
    Species.find()
     .and([
        {nl:  new RegExp(name, 'i')},
        {active:  true}
      ])
      .sort('nl')
      .exec(function (err, animals) {
        if(err) { return handleError(res, err); }
        else {return res.json(200, animals);}
      });
  }
  else{
    Species.find()
      .and([
        {en:  new RegExp(name, 'i')},
        {active:  true}
      ])
      .sort('en')
      .exec(function (err, animals) {
        if(err) { return handleError(res, err); }
        else {return res.json(200, animals);}
      });
  }
};

exports.findAllByName = function(req, res) {
  var name = key.escape(req.params.name);
  var lang = req.params.lang;
  if(lang === 'fr'){
    Species.find({fr:  new RegExp(name, 'i')})
      .sort('fr')
      .exec(function (err, animals) {
        if(err) { return handleError(res, err); }
        else {return res.json(200, animals);}
      });
  }
  else if(lang === 'nl'){
    Species.find({nl:  new RegExp(name, 'i')})
      .sort('nl')
      .exec(function (err, animals) {
        if(err) { return handleError(res, err); }
        else {return res.json(200, animals);}
      });
  }
  else{
    Species.find({en:  new RegExp(name, 'i')})
      .sort('en')
      .exec(function (err, animals) {
        if(err) { return handleError(res, err); }
        else {return res.json(200, animals);}
      });
  }
};


// Deletes a species from the DB.
exports.destroy = function(req, res) {
  Species.findById(key.escape(req.params.id), function (err, species) {
    if(err) { return handleError(res, err); }
    else if(!species) { return res.send(404); }
    else {
        species.remove(function(err) {
          if(err) { return handleError(res, err); }
          else {return res.send(204);}
        });
    }
  });
};


exports.index = function(req, res) {
     
  var lang = req.params.lang;
    
  if(lang === 'fr'){
    Species.find()
      .sort('fr')
    .exec(function (err, species) {
        if(err) { return handleError(res, err); }
        else {return res.json(200, species);}
      });
  }
  else if(lang === 'nl'){
    Species.find()
      .sort('nl')
    .exec(function (err, species) {
        if(err) { return handleError(res, err); }
        else {return res.json(200, species);}
      });
  }
  else{
    Species.find()
      .sort('en')
    .exec(function (err, species) {
        if(err) { return handleError(res, err); }
        else {return res.json(200, species);}
      });
  }
};


function handleError(res, err) {
  console.log(err);
  return res.send(500, err);
}
