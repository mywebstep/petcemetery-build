'use strict';

var express = require('express');
var controller = require('./species.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

//router.get('/', controller.index);
router.get('/:lang', auth.hasRole('admin'), controller.index);
router.put('/state/:id', auth.hasRole('admin'), controller.changeState);

router.get('/:id', controller.show);
router.post('/', auth.hasRole('admin'), controller.create);
router.put('/:id', auth.hasRole('admin'), controller.update);
router.patch('/:id', auth.hasRole('admin'), controller.update);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);
router.get('/findbyname/:name/:lang', controller.findByName);
router.get('/findallbyname/:name/:lang', auth.hasRole('admin'), controller.findAllByName);

module.exports = router;
