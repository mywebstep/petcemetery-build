'use strict';

var express = require('express');
var controller = require('./contact.controller');


var router = express.Router();

router.post('/', controller.message);

module.exports = router;