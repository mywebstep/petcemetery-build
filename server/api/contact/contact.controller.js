'use strict';

var _ = require('lodash');
var sendgrid  = require('sendgrid')('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');

var https = require('https');

var SECRET = "6Le8XwsTAAAAAJaQBgshEH75ijtzpjJ04TYrKQQ-";

// Deletes a contact from the DB.
exports.message = function(req, res) {
    
    verifyRecaptcha(req.body.captcha, function(success) {
                if (success) {
                    var text = req.body.message.replace(/(?:\r\n|\r|\n)/g, '<br />');
                    var message = 'Message de: ' +  req.body.userName + ' '+ req.body.firstName + '<br>' + 'Entreprise: '+ req.body.company+ '<br>'  + 'Téléphone: '+ req.body.phone + '<br>'  + 'Message: ' + '<br>'  +  text + '<br>'; 
                    sendgrid.send({
                      to:       'admin@animal-paradise.eu',
                      from:     req.body.email,
                      subject:  req.body.subject,
                      html:     message
                    }, function(err, json) {
                      if (err) { 
                          console.error(err); 
                          return res.send(500, "problem during sending message..");
                      }
                      //console.log(json);
                    });
                    res.send(200);
                } else {
                    return res.send(409, "Captcha failed, sorry.");
                }
        });
    
    

};

function verifyRecaptcha(key, callback) {
        https.get("https://www.google.com/recaptcha/api/siteverify?secret=" + SECRET + "&response=" + key, function(res) {
                var data = "";
                res.on('data', function (chunk) {
                        data += chunk.toString();
                });
                res.on('end', function() {
                    try {
                            var parsedData = JSON.parse(data);
                            return callback(parsedData.success);
                    } catch (e) {
                            return callback(false);
                    }
                });
        });
}

function handleError(res, err) {
  console.log(err);
  return res.send(500, err);
}