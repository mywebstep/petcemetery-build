'use strict';

var express = require('express');
var controller = require('./aws.controller');

var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/sign_s3', auth.isAuthenticated(), controller.signS3);

module.exports = router;