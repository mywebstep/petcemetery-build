'use strict';

var _ = require('lodash');
var aws = require('aws-sdk');
var uuid = require('node-uuid');

aws.config.region ='eu-west-1';

var AWS_ACCESS_KEY = process.env.AWS_ACCESS_KEY;
var AWS_SECRET_KEY = process.env.AWS_SECRET_KEY;
var S3_BUCKET = process.env.S3_BUCKET

// Get list of awss
exports.signS3 = function(req, res) {
  //TODO signatuerVersion why? mandatory?
  //TODO limit user request by day
  aws.config.update({accessKeyId: AWS_ACCESS_KEY, secretAccessKey: AWS_SECRET_KEY, signatureVersion: 'v4', region: 'eu-west-1'});
    var s3 = new aws.S3();
    var s3_object_name = uuid.v4();
    var key = '';
    if(req.query.type && req.query.type === 'gift'){
        key = "gift/"+s3_object_name;
    }
    else {
        key = "animal/"+s3_object_name;
    }
    var s3_params = {
        Bucket: S3_BUCKET,
        Key: key,
        Expires: 60,//in seconds
        ContentType: req.query.s3_object_type,
        ACL: 'public-read'
    };
    s3.getSignedUrl('putObject', s3_params, function(err, data){
        if(err){
            return handleError(res, err);
        }
        else{
            var url = '';
            if(req.query.type && req.query.type === 'gift'){
                url = 'https://'+S3_BUCKET+'.s3.amazonaws.com/gift/'+s3_object_name
            }
            else{
                url = 'https://'+S3_BUCKET+'.s3.amazonaws.com/animal/'+s3_object_name
            }
            var return_data = {
                signedRequest: data,
                url: url
            };
            res.write(JSON.stringify(return_data));
            res.end();
        }
    });
};

function handleError(res, err) {
  console.log(err);
  return res.send(500, err);
}
