'use strict';

var express = require('express');
var controller = require('./hipay.controller');

var router = express.Router();


router.get('/notify', controller.notifyGiftCreation);
router.get('/notifygift', controller.notifyGiftCreation);
router.get('/notifyanimal', controller.notifyAnimalCreation);


module.exports = router;