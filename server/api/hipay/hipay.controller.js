'use strict';

var _ = require('lodash');
var paymentController = require('../payment/payment.controller');
var hipayKey = 'cb01ade2560b91a3e11b3124afe76128';

exports.notifyAnimalCreation = function(req, res) {
  var action = req.query.action;
  var status = req.query.status;
  var animalId = req.query.data;
  var amount = req.query.reference_amount;
    
  /* reference_amount is 0 iwth ocde test */
  if(req.query.test) {
      if(req.query.product_name === '5-CREATION')
        amount = 5;
      if(req.query.product_name === '10-CREATION')
        amount = 10;
      if(req.query.product_name === '15-CREATION')
        amount = 15;
  }
      
  if(verifySignature(req.query) && action === "payment-confirm" && status === "0"){
      paymentController.receivePaymentAfterAnimalCreation(animalId, amount, function(credited, activated, message){
          if(!activated){
            console.log(message);   
          }
          if(credited){
            return res.send(200);
          }
          if(!credited){
            return handleError(res, message);  
          }
      });    
  }
  else {
    return handleError(res, 'Error during signature verification');
  }
};

exports.notifyGiftCreation = function(req, res) {
  var action = req.query.action;
  var status = req.query.status;
  var animalId = req.query.data;
  var giftId = req.query.merchant_transaction_id;
  var amount = req.query.reference_amount;
  
  /* reference_amount is 0 iwth ocde test */
  if(req.query.test) {
      if(req.query.product_name === '5-GIFT')
        amount = 5;
      if(req.query.product_name === '10-GIFT')
        amount = 10;
      if(req.query.product_name === '15-GIFT')
        amount = 15;
  }
  if(verifySignature(req.query) && action === "payment-confirm" && status === "0"){ 
      paymentController.receivePaymentAfterGiftCreation(animalId, giftId, amount, function(credited, activated, message){
          if(!activated){
            console.log(message);
            return res.send(200);
          }
          if(credited){
            return res.send(200);
          }
          if(!credited){
            return handleError(res, message);  
          }
      });
  }
  else {
    return handleError(res, 'Error during signature verification');
  }
};


function handleError(res, err) {
  console.log('Hipay - '+err);
  return res.send(500, err);
}

function verifySignature(query){
  var signature = query.api_sig;
  var sortedProperties = ksort(query);
  var stringToCompute = '';
    
  for (var propName in sortedProperties) {
    if (sortedProperties.hasOwnProperty(propName)  && propName !== 'api_sig') {
        stringToCompute = stringToCompute+propName+sortedProperties[propName];
    }
  }
  stringToCompute =stringToCompute+hipayKey;

  var computedSignature = require('crypto').createHash('sha1').update(stringToCompute).digest('hex');
    
  return computedSignature === signature;
    
}

function ksort(w) 
{ 
  var sArr = []; 
  var tArr = []; 
  var n = 0; 
  for (var i in w) 
  { 
    tArr[n++] = i; 
  } 
  //tri du plus petit au plus grand 
  tArr = tArr.sort(); 
  n = tArr.length; 
  for (var j=0; j<n; j++) { 
    sArr[tArr[j]] = w[tArr[j]]; 
  } 
  return sArr; 
}