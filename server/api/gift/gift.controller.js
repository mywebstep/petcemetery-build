'use strict';

var _ = require('lodash');
var Gift = require('./gift.model');
var key = require('mongo-key-escape');

// Get list of gifts
exports.index = function(req, res) {
  Gift.find({active:  true}).sort('price').exec(function (err, gifts) {
    if(err) { return handleError(res, err); }
    else {return res.json(200, gifts);}
  });
};

exports.getAll = function(req, res) {
  Gift.find(function (err, gifts) {
    if(err) { return handleError(res, err); }
    else {return res.json(200, gifts);}
  }).sort('price');
};

// Get a single gift
exports.show = function(req, res) {
  Gift.findById(key.escape(req.params.id), function (err, gift) {
    if(err) { return handleError(res, err); }
    else if(!gift) { return res.send(404); }
    else {return res.json(gift);}
  });
};

// Creates a new gift in the DB.
exports.create = function(req, res) { 
  Gift.create(new Gift(req.body), function(err, gift) {
    if(err) { return handleError(res, err); }
    else {return res.json(201, gift);}
  });
};

// Updates an existing gift in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Gift.findById(key.escape(req.params.id), function (err, gift) {
    if (err) { return handleError(res, err); }
    else if(!gift) { return res.send(404); }
    else {
        var updated = _.merge(gift, req.body);
        updated.save(function (err) {
          if (err) { return handleError(res, err); }
          else {return res.json(200, gift);}
        });
    }
  });
};

// Deletes a gift from the DB.
exports.destroy = function(req, res) {
  Gift.findById(key.escape(req.params.id), function (err, gift) {
    if(err) { return handleError(res, err); }
    else if(!gift) { return res.send(404); }
    else{
        gift.remove(function(err) {
          if(err) { return handleError(res, err); }
          else {return res.send(204);}
        });
    }
  });
};

exports.changeState = function(req, res) {
   var giftId = key.escape(req.params.id);
   Gift.findById(giftId, function (err, gift) {
      gift.active = !gift.active;
      gift.save(function(err) {
        if (err) {return handleError(res, err); }
        else {res.send(200);}
      });
    
  });
};

function handleError(res, err) {
  console.log(err);
  return res.send(500, err);
}