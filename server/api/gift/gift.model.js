'use strict';

var mongoose = require('mongoose'),
    validators = require('mongoose-validators'),
    Schema = mongoose.Schema;

var GiftSchema = new Schema({
  image: { type: String, validate: validators.isURL({require_tld: false}), required: true},
  price: { type: Number, min:0, required: true},
  active: { type: Boolean, default: true , required: true},
});

module.exports = mongoose.model('Gift', GiftSchema);