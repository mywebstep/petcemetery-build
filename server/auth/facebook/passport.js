var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;

exports.setup = function (User, config) {
  passport.use(new FacebookStrategy({
      clientID: config.facebook.clientID,
      clientSecret: config.facebook.clientSecret,
      callbackURL: config.facebook.callbackURL,
      profileFields: ['id', 'displayName', 'emails']
    },
    function(accessToken, refreshToken, profile, done) {
      User.findOne({
        'facebook.id': profile.id
      },
      function(err, user) {
        if (err) {
          return done(err);
        }
        if (user && user.disable) {
          return done(null, false, { message: 'This user is disabled.' });
        } 
        if (!user) {
            var mail ='';
          if(!profile.emails || !profile.emails[0] || !profile.emails[0].value){
              mail= profile.id+'@facebook.com';
          }
          else{
              mail = profile.emails[0].value;
          }
        
          user = new User({
            name: profile.displayName,
            email: mail,
            role: 'user',
            provider: 'facebook',
            facebook: profile._json
          });
          user.save(function(err) {
            if (err) done(err);
            return done(err, user);
          });
        } else {
          return done(err, user);
        }
      })
    }
  ));
};