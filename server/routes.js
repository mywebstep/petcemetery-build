/**
 * Main application routes
 */

'use strict';

var errors = require('./components/errors');

module.exports = function(app) {

  // Insert routes below
  app.use('/api/braintree', require('./api/braintree'));
  app.use('/api/hipay', require('./api/hipay'));
  app.use('/api/contacts', require('./api/contact'));
  app.use('/api/gifts', require('./api/gift'));
  app.use('/api/species', require('./api/species'));
  app.use('/api/aws', require('./api/aws'));
  app.use('/api/users', require('./api/user'));
  app.use('/api/animals', require('./api/animal'));
  

  app.use('/auth', require('./auth'));
  
  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
   .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get(function(req, res) {
      res.sendfile(app.get('appPath') + '/index.html');
    });
};
