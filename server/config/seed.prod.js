/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var Animal = require('../api/animal/animal.model');
var User = require('../api/user/user.model');
var Species = require('../api/species/species.model');
var Gift = require('../api/gift/gift.model');
/*
Gift.find({}).remove(function() {
  Gift.create({
    image : 'assets/images/gift/0e264894.gift1.png',
    price : 0
  }, {
    image : 'assets/images/gift/49a8dab6.gift2.png',
    price : 0
  }, {
    image : 'assets/images/gift/3a9dd476.gift3.png',
    price : 0.3
  }, {
    image : 'assets/images/gift/2398ae49.gift4.png',
    price : 0.3
  }, {
    image : 'assets/images/gift/20f5d9a8.gift5.png',
    price : 0.3
  }, {
    image : 'assets/images/gift/e627d3f7.gift6.png',
    price : 0.3
  }, {
    image : 'assets/images/gift/ead6c82e.gift7.png',
    price : 0.3
  }, {
    image : 'assets/images/gift/d0a165b8.gift8.png',
    price : 0.3
  }, {
    image : 'assets/images/gift/c89161e4.gift9.png',
    price : 0.5
  }, {
    image : 'assets/images/gift/f143e274.gift10.png',
    price : 0.5
  }, {
    image : 'assets/images/gift/e1ffed3e.gift11.png',
    price : 0.5
  }, {
    image : 'assets/images/gift/7db15bb3.gift12.png',
    price : 0.5
  }, {
    image : 'assets/images/gift/bc99fcb5.gift13.png',
    price : 0.8
  }, {
    image : 'assets/images/gift/cf88321f.gift14.png',
    price : 0.8
  }, {
    image : 'assets/images/gift/961fcae2.gift15.png',
    price : 0.8
  });
});*/

/*
Species.find({}).remove(function() {
  Species.create({
    active: true,
    fr : 'chien',
    nl : 'hond',
    en : 'dog'
  }, {
    active: true,
    fr : 'chat',
    nl : 'kat',
    en : 'cat'
  }, {
    active: true,
    fr : 'ours',
    nl : 'beer',
    en : 'bear'
  },  {
    active: true,
    fr : 'poule',
    nl : 'kip',
    en : 'chicken'
  },  {
    active: true,
    fr : 'mouton',
    nl : 'schaap',
    en : 'sheep'
  },{
    active: true,
    fr : 'lion',
    nl : 'leeuw',
    en : 'lion'
  },{
    active: true,
    fr : 'dindon',
    nl : 'kalkoen',
    en : 'turkey'
  },{
    fr : 'hamster',
    nl : 'hamster',
    en : 'hamster'
  },{
    active: true,
    fr : 'girafe',
    nl : 'giraf',
    en : 'giraffe'
  },{
    active: true,
    fr : 'poussin',
    nl : 'kuiken',
    en : 'chick'
  },{
    active: true,
    fr : 'lapin',
    nl : 'konijn',
    en : 'rabbit'
  });
});*/

//Animal.find({}).remove(function() {});

