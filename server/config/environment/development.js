'use strict';

// Development specific configuration
// ==================================
module.exports = {
  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/petcemetery-dev'
  },
  https: false,
  seedDB: true
};
