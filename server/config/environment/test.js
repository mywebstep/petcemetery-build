'use strict';

// Test specific configuration
// ===========================
module.exports = {
  // MongoDB connection options
https: false,
  mongo: {
    uri: 'mongodb://localhost/petcemetery-test'
  }
};