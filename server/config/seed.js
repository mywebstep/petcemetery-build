/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var Animal = require('../api/animal/animal.model');
var User = require('../api/user/user.model');
var Species = require('../api/species/species.model');
var Gift = require('../api/gift/gift.model');

Gift.find({}).remove(function() {
  Gift.create({
    _id: '5569d96352dd199415fe5019',
    image : 'assets/images/gift/0e264894.gift1.png',
    price : 0
  }, {
    _id: '5569d96352dd199415fe501a',
    image : 'assets/images/gift/49a8dab6.gift2.png',
    price : 0
  }, {
    _id: '5569d96352dd199415fe501b',
    image : 'assets/images/gift/3a9dd476.gift3.png',
    price : 0.3
  }, {
    _id: '5569d96352dd199415fe501c',
    image : 'assets/images/gift/2398ae49.gift4.png',
    price : 0.3
  }, {
    _id: '5569d96352dd199415fe501d',
    image : 'assets/images/gift/20f5d9a8.gift5.png',
    price : 0.3
  }, {
    _id: '5569d96352dd199415fe501e',
    image : 'assets/images/gift/e627d3f7.gift6.png',
    price : 0.3
  }, {
    _id: '5569d96352dd199415fe501f',
    image : 'assets/images/gift/ead6c82e.gift7.png',
    price : 0.3
  }, {
    _id: '5569d96352dd199415fe5020',
    image : 'assets/images/gift/d0a165b8.gift8.png',
    price : 0.3
  }, {
    _id: '5569d96352dd199415fe5021',
    image : 'assets/images/gift/c89161e4.gift9.png',
    price : 0.5
  }, {
    _id: '5569d96352dd199415fe5022',
    image : 'assets/images/gift/f143e274.gift10.png',
    price : 0.5
  }, {
    _id: '5569d96352dd199415fe5023',
    image : 'assets/images/gift/e1ffed3e.gift11.png',
    price : 0.5
  }, {
    _id: '5569d96352dd199415fe5024',
    image : 'assets/images/gift/7db15bb3.gift12.png',
    price : 0.5
  }, {
    _id: '5569d96352dd199415fe5025',
    image : 'assets/images/gift/bc99fcb5.gift13.png',
    price : 0.8
  }, {
    _id: '5569d96352dd199415fe5026',
    image : 'assets/images/gift/cf88321f.gift14.png',
    price : 0.8
  }, {
    _id: '5569d96352dd199415fe5027',
    image : 'assets/images/gift/961fcae2.gift15.png',
    price : 0.8
  });
});


Species.find({}).remove(function() {
  Species.create({
    _id: '555cd408ceac6a6d6a6c0dec',
    active: true,
    fr : 'chien',
    nl : 'hond',
    en : 'dog'
  }, {
    _id: '555cd408ceac6a6d6a6c0ded',
    active: true,
    fr : 'chat',
    nl : 'kat',
    en : 'cat'
  }, {
    _id: '555cd408ceac6a6d6a6c0dee',
    active: true,
    fr : 'ours',
    nl : 'beer',
    en : 'bear'
  },  {
    _id: '555cd408ceac6a6d6a6c0def',
    active: true,
    fr : 'poule',
    nl : 'kip',
    en : 'chicken'
  },  {
    _id: '555cd408ceac6a6d6a6c0df0',
    active: true,
    fr : 'mouton',
    nl : 'schaap',
    en : 'sheep'
  },{
    _id: '555cd408ceac6a6d6a6c0df1',
    active: true,
    fr : 'lion',
    nl : 'leeuw',
    en : 'lion'
  },{
    _id: '555cd408ceac6a6d6a6c0df2',
    active: true,
    fr : 'dindon',
    nl : 'kalkoen',
    en : 'turkey'
  },{
    _id: '555cd408ceac6a6d6a6c0df3',
    fr : 'hamster',
    nl : 'hamster',
    en : 'hamster'
  },{
    _id: '555cd408ceac6a6d6a6c0df4',
    active: true,
    fr : 'girafe',
    nl : 'giraf',
    en : 'giraffe'
  },{
    _id: '555cd408ceac6a6d6a6c0df5',
    active: true,
    fr : 'poussin',
    nl : 'kuiken',
    en : 'chick'
  },{
    _id: '555cd408ceac6a6d6a6c0df6',
    active: true,
    fr : 'lapin',
    nl : 'konijn',
    en : 'rabbit'
  });
});

Animal.find({}).remove(function() {
  Animal.create({
    _id: '550467e8bfd12eac154ad6af',
    name: 'Dandy',
    sex: 'M',
    birthDate : new Date(2010, 1, 4),
    deathDate : new Date(2014, 1, 19),
    creationDate: new Date(2015, 2, 15),
    species: '555cd408ceac6a6d6a6c0dec',
    race: 'cocker',
    photoUrl: 'assets/images/animals/d9170fdd.dog12.jpg',
    resources: [{url: 'assets/images/animals/c2c8bd62.cocker1.jpg', active: true}, {url: 'assets/images/animals/3f2ea6f2.cocker2.jpg', active: true}, {url: 'assets/images/animals/103749a1.cocker3.jpg', active: false}, {url: 'assets/images/animals/7a10b4f5.cocker4.jpg', active: true}],
    buyingResoures: 2,
    description: '« J’ai vu au fond de tes yeux et de ton regard l’amour qu’on ne lit nulle part. Souvent j’y ai vu la tendresse , de l’affection mais aussi la tristesse. Mais j’y ai vu surtout l’amour que tu me donnais jour après jour »',
    descriptionAuthor: 'Julie Delplace',
    video: 'https://www.youtube.com/embed/IqWuW3wZZzk',
    active: true,
    creator:'650467e8bfd12eac154ad6af',
    gifts: [
        {gift: '5569d96352dd199415fe5019', message: 'En dernier hommage à Dandy', date: new Date(2015, 3, 17), creator:'650467e8bfd12eac154ad6af', active: true},
        {gift: '5569d96352dd199415fe501a', message: 'Tendrement', date: new Date(2015, 3, 21), creator:'750467e8bfd12eac154ad6af', active: true},
        {gift: '5569d96352dd199415fe501b', message: 'Pour toi Dandy', date: new Date(2015, 3, 27), creator:'650467e8bfd12eac154ad6af', active: true},
        {gift: '5569d96352dd199415fe501c', message: 'A la mémoire de Dandy', date: new Date(2015, 1, 21), creator:'750467e8bfd12eac154ad6af', active: true},
        {gift: '5569d96352dd199415fe501d', message: 'En hommage', date: new Date(2015, 3, 26), creator:'750467e8bfd12eac154ad6af', active: true},
        {gift: '5569d96352dd199415fe501e', message: 'Pour toi', date: new Date(2015, 3, 1), creator:'650467e8bfd12eac154ad6af', active: true},
        {gift: '5569d96352dd199415fe501f', message: 'Avec toute mon affection', date: new Date(2015, 2, 2), creator:'750467e8bfd12eac154ad6af', active: true},
        {gift: '5569d96352dd199415fe5020', message: 'Nous ne t’oublierons pas', date: new Date(2015, 1, 5), creator:'650467e8bfd12eac154ad6af', active: true},
        {gift: '5569d96352dd199415fe5021', message: 'A notre chien', date: new Date(2015, 0, 7), creator:'750467e8bfd12eac154ad6af', active: true},
        {gift: '5569d96352dd199415fe5022', message: 'A toi mon beau', date: new Date(2014, 3, 9), creator:'650467e8bfd12eac154ad6af', active: true},
        {gift: '5569d96352dd199415fe5023', message: 'Eternellement', date: new Date(2014, 5, 8), creator:'750467e8bfd12eac154ad6af', active: true},
        {gift: '5569d96352dd199415fe5024', message: 'Nous vous soutenons dans votre tristesse', date: new Date(2014, 7, 6), creator:'750467e8bfd12eac154ad6af', active: true},
        {gift: '5569d96352dd199415fe5025', message: 'Affectueusement', date: new Date(2014, 9, 5), creator:'650467e8bfd12eac154ad6af', active: true},
        {gift: '5569d96352dd199415fe5026', message: 'Pour ce magnifique chien', date: new Date(2014,11, 17), creator:'650467e8bfd12eac154ad6af', active: true},
        {gift: '5569d96352dd199415fe5027', message: 'Un cocker hors norme', date: new Date(2014, 10, 18), creator:'750467e8bfd12eac154ad6af', active: true},
    ]
  },{
    _id: '550467e8bfd12eac154ad6b0',
    creator:'650467e8bfd12eac154ad6af',
    name: 'Toby',
    sex: 'M',
    birthDate : new Date(2007, 1, 7),
    deathDate : new Date(2011, 1, 16),
    creationDate: new Date(2015, 2, 14),
    species: '555cd408ceac6a6d6a6c0dec',
    race: 'caniche',
    photoUrl: 'assets/images/animals/70f42600.dog14.jpg',
    description: 'Nous ne t’oublierons jamais notre Toby. C’était un chien tellement affectueux et mignon comme tout. Tu nous manques déjà terriblement. La maison va être bien vide sans toi.Tu seras dans notre coeur éternellement. Repose en paix, notre amour de chien.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Barry',
    sex: 'M',
    birthDate : new Date(2005, 11, 8),
    deathDate : new Date(2019, 11, 9),
    creationDate: new Date(2015, 2, 12),
    species: '555cd408ceac6a6d6a6c0dec',
    race: 'chihuahua',
    photoUrl: 'assets/images/animals/1d28ba95.dog1.jpg',
    description: 'Quand tu es heureux, regarde au plus profond de toi. Tu verras que seul ce qui t’apporte de la peine, t’apporte aussi de la joie. Quand tu es triste, regarde à nouveau dans ton coeur, et tu verras que tu pleures ce qui te rendait heureux.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'650467e8bfd12eac154ad6af',
    name: 'Panpan',
    sex: 'F',
    birthDate : new Date(1999, 0, 17),
    deathDate : new Date(2003, 11, 15),
    creationDate: new Date(2015, 2, 11),
    species: '555cd408ceac6a6d6a6c0dec',
    race: 'border collie',
    photoUrl: 'assets/images/animals/7d8101f5.dog2.jpg',
    description: 'La mort tombe dans la vie comme une pierre dans un étang : d’abord, éclaboussures, affolements dans les buissons, battements d’ailes et fuites en tout sens. Ensuite, grands cercles sur l’eau, de plus en plus larges. Enfin le calme à nouveau, mais pas du tout le même silence qu’auparavant, un silence, comment dire : assourdissant.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'650467e8bfd12eac154ad6af',
    name: 'Pablo',
    sex: 'M',
    birthDate : new Date(2010, 2, 19),
    deathDate : new Date(2013, 10, 17),
    creationDate: new Date(2015, 2, 10),
    species: '555cd408ceac6a6d6a6c0dec',
    race: 'akita Inu',
    photoUrl: 'assets/images/animals/9d67c5ad.dog3.jpg',
    description: 'Parler de ses peines, c’est déjà se consoler ',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Calinou',
    sex: 'F',
    birthDate : new Date(1985, 3, 16),
    deathDate : new Date(2005, 9, 14),
    creationDate: new Date(2015, 2, 9),
    species: '555cd408ceac6a6d6a6c0dec',
    race: 'caniche',
    photoUrl: 'assets/images/animals/068faf23.dog4.jpg',
    description: 'L’esprit oublie toutes les souffrances quand le chagrin a des compagnons et que l’amitié le console.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Scott',
    sex: 'M',
    birthDate : new Date(2007, 9, 19),
    deathDate : new Date(2011, 4, 15),
    creationDate: new Date(2015, 2, 9),
    species: '555cd408ceac6a6d6a6c0dec',
    race: 'yorkshire terrier',
    photoUrl: 'assets/images/animals/b3eb4e4a.dog5.jpg',
    description: 'Pleurer a toujours été pour moi un moyen de sortir les choses profondément enfouies. Quand je chante, je pleure souvent. Pleurer, c’est ressentir, c’est être humain.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Nala',
    sex: 'F',
    birthDate : new Date(1990, 7, 17),
    deathDate : new Date(2000, 9, 19),
    creationDate: new Date(2015, 2, 8),
    species: '555cd408ceac6a6d6a6c0dec',
    race: 'Shiba Inu',
    photoUrl: 'assets/images/animals/75318d22.dog6.jpg',
    description: 'La mort est une surprise que fait l’inconcevable au concevable.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Simba',
    sex: 'M',
    birthDate : new Date(1999, 9, 19),
    deathDate : new Date(2011, 4, 15),
    creationDate: new Date(2015, 2, 8),
    species: '555cd408ceac6a6d6a6c0ded',
    race: 'sibérien',
    photoUrl: 'assets/images/animals/4680a3ec.cat1.jpg',
    description: 'Ne mettez pas d’obstacle au mouvement de la douleur. Laissez-le mûrir.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Tompaze',
    sex: 'F',
    birthDate : new Date(1987, 2, 6),
    deathDate : new Date(1996, 10, 14),
    creationDate: new Date(2015, 2, 8),
    species: '555cd408ceac6a6d6a6c0def',
    photoUrl: 'assets/images/animals/b9991ed2.chicken1.jpg',
    description: 'Nul ne peut atteindre l’aube sans passer par le chemin de la nuit',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Mystère',
    sex: 'F',
    birthDate : new Date(2002, 10, 16),
    deathDate : new Date(2004, 1, 16),
    creationDate: new Date(2015, 2, 7),
    species: '555cd408ceac6a6d6a6c0ded',
    race: 'siamois',
    photoUrl: 'assets/images/animals/ef7c54f8.cat2.jpg',
    description: 'Nous ne t’oublierons jamais notre Toby. C’était un chien tellement affectueux et mignon comme tout. Tu nous manques déjà terriblement. La maison va être bien vide sans toi.Tu seras dans notre coeur éternellement. Repose en paix, notre amour de chien.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Bebe',
    sex: 'F',
    birthDate : new Date(2004, 11, 17),
    deathDate : new Date(2011, 1, 15),
    creationDate: new Date(2015, 2, 6),
    species: '555cd408ceac6a6d6a6c0ded',
    race: 'siamois',
    photoUrl: 'assets/images/animals/6d801e13.cat3.jpg',
    description: 'Tristesse ! On passe la moitié de la vie à attendre ceux qu’on aimera et l’autre moitié à quitter ceux qu’on aime.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Voxy',
    sex: 'M',
    birthDate : new Date(2007, 10, 17),
    deathDate : new Date(2013, 6, 15),
    creationDate: new Date(2015, 2, 5),
    species: '555cd408ceac6a6d6a6c0df0',
    photoUrl: 'assets/images/animals/6d96c599.sheep1.jpg',
    description: 'Tu n’es plus là où tu étais, mais tu es partout là où je suis.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Chipy',
    sex: 'F',
    birthDate : new Date(1975, 6, 17),
    deathDate : new Date(1985, 11, 14),
    creationDate: new Date(2015, 2, 5),
    species: '555cd408ceac6a6d6a6c0dec',
    race: 'akita inu',
    photoUrl: 'assets/images/animals/83d0b012.dog7.jpg',
    description: 'La guérison n’est jamais si prompte que la blessure.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Fifille',
    sex: 'F',
    birthDate : new Date(1966, 4, 16),
    deathDate : new Date(1969, 0, 12),
    creationDate: new Date(2015, 2, 5),
    species: '555cd408ceac6a6d6a6c0dee',
    photoUrl: 'assets/images/animals/98b2517c.bear1.jpg',
    description: 'L’arbre qui s’écroule fait beaucoup plus de bruit que la forêt qui pousse.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Ubac',
    sex: 'M',
    birthDate : new Date(1999, 5, 17),
    deathDate : new Date(2004, 7, 15),
    creationDate: new Date(2015, 2, 5),
    species: '555cd408ceac6a6d6a6c0ded',
    race: 'persan',
    photoUrl: 'assets/images/animals/45cb8c52.cat4.jpg',
    description: 'Même quand la blessure guérit, la cicatrice demeure.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Shiva',
    sex: 'M',
    birthDate : new Date(2010, 9, 14),
    deathDate : new Date(2011, 11, 19),
    creationDate: new Date(2015, 2, 4),
    species: '555cd408ceac6a6d6a6c0dee',
    photoUrl: 'assets/images/animals/27623ce7.bear2.jpg',
    description: 'Une personne est morte et définitivement morte ce jour ou tu as décidé de faire le grand saut, c’est mon ancien moi. Par ton acte, tu m’as obligée a mourir à moi même, puis à renaître de mes cendres et de par ma renaissance te voici immortelle.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Princesse',
    sex: 'F',
    birthDate : new Date(2000, 6, 19),
    deathDate : new Date(2003, 9, 1),
    creationDate: new Date(2015, 2, 3),
    species: '555cd408ceac6a6d6a6c0df1',
    photoUrl: 'assets/images/animals/ef847b4d.lion1.jpg',
    description: 'Ce qui compte, ce ne sont pas les années qu’il y a eu dans la vie. C’est la vie qu’il y a eu dans les années.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Sofia',
    sex: 'F',
    birthDate : new Date(2011, 9, 15),
    deathDate : new Date(2013, 6, 19),
    creationDate: new Date(2015, 2, 2),
    species: '555cd408ceac6a6d6a6c0dec',
    race: 'jack russell terrier',
    photoUrl: 'assets/images/animals/c33ed654.dog8.jpg',
    description: 'Mieux vaut souffrir d’avoir aimé que de souffrir de n’avoir jamais aimé.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Roxie',
    sex: 'F',
    birthDate : new Date(1995, 7, 17),
    deathDate : new Date(1999, 6, 13),
    creationDate: new Date(2015, 2, 2),
    species: '555cd408ceac6a6d6a6c0dec',
    race: 'pinscher',
    photoUrl: 'assets/images/animals/e9cfe5b5.dog9.jpg',
    description: 'Le souvenir est le parfum de l’âme',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Rica',
    sex: 'F',
    birthDate : new Date(1987, 9, 18),
    deathDate : new Date(1989, 4, 1),
    creationDate: new Date(2015, 2, 2),
    species: '555cd408ceac6a6d6a6c0ded',
    race: 'british shorthair',
    photoUrl: 'assets/images/animals/17245a6e.cat5.jpg',
    description: 'Le souvenir, c’est la présence invisible.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Zorino',
    sex: 'M',
    birthDate : new Date(1978, 8, 18),
    deathDate : new Date(1985, 10, 14),
    creationDate: new Date(2015, 2, 2),
    species: '555cd408ceac6a6d6a6c0df2',
    photoUrl: 'assets/images/animals/5f917c4d.turkey1.jpg',
    description: 'Rien n’est plus vivant qu’un souvenir.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Pepsy',
    sex: 'F',
    birthDate : new Date(1979, 6, 18),
    deathDate : new Date(1982, 2, 1),
    creationDate: new Date(2015, 2, 1),
    species: '555cd408ceac6a6d6a6c0df1',
    photoUrl: 'assets/images/animals/be91cc2b.lion2.jpg',
    description: 'Le respect est un hommage qu’on rend sans peine à la vertu',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Iris',
    sex: 'F',
    birthDate : new Date(1984, 9, 1),
    deathDate : new Date(2000, 11, 17),
    creationDate: new Date(2015, 2, 1),
    species: '555cd408ceac6a6d6a6c0df3',
    photoUrl: 'assets/images/animals/ff40f5e2.hamster1.jpg',
    description: 'L’hommage des amis, c’est la fidélité.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Plume',
    sex: 'F',
    birthDate : new Date(1998, 6, 1),
    deathDate : new Date(2011, 4, 13),
    creationDate: new Date(2015, 2, 1),
    species: '555cd408ceac6a6d6a6c0dec',
    race: 'dogue argentin',
    photoUrl: 'assets/images/animals/98110b8a.dog10.jpg',
    description: 'On rend d’autant mieux hommage à un mort, qu’on n’a plus à l’envier.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Simba',
    sex: 'M',
    birthDate : new Date(2004, 9, 1),
    deathDate : new Date(2011, 11, 1),
    creationDate: new Date(2015, 2, 1),
    species: '555cd408ceac6a6d6a6c0ded',
    race: 'chartreux',
    photoUrl: 'assets/images/animals/1a019233.cat6.jpg',
    description: 'Nous ne t’oublierons jamais.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Nerod',
    sex: 'M',
    birthDate : new Date(2007, 10, 8),
    deathDate : new Date(2014, 1, 6),
    creationDate: new Date(2015, 2, 1),
    species: '555cd408ceac6a6d6a6c0df4',
    photoUrl: 'assets/images/animals/13706ae8.giraffe1.jpg',
    description: 'La mort est un fidel ami qui t’accompagne dès ta naissance mais te prive de la vie',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Spike',
    sex: 'M',
    birthDate : new Date(2001, 4, 18),
    deathDate : new Date(2014, 6, 1),
    creationDate: new Date(2015, 1, 29),
    species: '555cd408ceac6a6d6a6c0df5',
    photoUrl: 'assets/images/animals/10c917cb.chick1.jpg',
    description: 'La mort est un principe de métamorphose non pas de fatalité. ',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Patou',
    sex: 'M',
    birthDate : new Date(2000, 6, 1),
    deathDate : new Date(2011, 1, 17),
    creationDate: new Date(2015, 1, 29),
    species: '555cd408ceac6a6d6a6c0dec',
    race: 'cane corso',
    photoUrl: 'assets/images/animals/c53a17a1.dog11.jpg',
    description: 'La mort est le regret de la vie.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Titus',
    sex: 'M',
    birthDate : new Date(2008, 2, 18),
    deathDate : new Date(2012, 11, 1),
    creationDate: new Date(2015, 1, 28),
    species: '555cd408ceac6a6d6a6c0dec',
    race: 'carlin',
    photoUrl: 'assets/images/animals/a125742b.dog13.jpg',
    description: 'La curiosité est indispensable, elle diminue la peur de la Mort.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Mimouche',
    sex: 'F',
    birthDate : new Date(2003, 10, 18),
    deathDate : new Date(2013, 6, 14),
    creationDate: new Date(2015, 1, 28),
    species: '555cd408ceac6a6d6a6c0df6',
    photoUrl: 'assets/images/animals/85097f43.rabbit1.jpg',
    description: 'Les morts sont des invisibles, mais non des absents.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Tomy',
    sex: 'M',
    birthDate : new Date(2002, 2, 15),
    deathDate : new Date(2014, 5, 16),
    creationDate: new Date(2015, 1, 28),
    species: '555cd408ceac6a6d6a6c0df6',
    photoUrl: 'assets/images/animals/b0e86790.rabbit2.jpg',
    description: 'J’ai reconnu le bonheur au bruit qu’il a fait en partant.',
    descriptionAuthor: 'Julie Delplace',
    active: true
  },{
    creator:'750467e8bfd12eac154ad6af',
    name: 'Sally',
    sex: 'F',
    birthDate : new Date(2000, 6, 12),
    deathDate : new Date(2011, 9, 20),
    creationDate: new Date(2015, 1, 27),
    species: '555cd408ceac6a6d6a6c0df3',
    photoUrl: 'assets/images/animals/e7911da2.hamster2.jpg',
    description: 'hjsqb cjhbds cjksdb csjhdb csjbjsd cshjdbcjhdsbc jsdkbc jdshbcjsdbc jdsbcjsd chbdhjsbcjdhsb cdjsb cjhdsbhcdhjsbcjhsdbcjdhsbcjhdsb cjhsdbc sdhjcbsdhjbc sjdhbcdhjsb jdbcjsdhbcjdhs cbdhjsbcsjdhb cshdsbchsbd cdhbsc jdhsbcdhjsbc ds chbdschjbdsjhbcdshjbcdhsjbcjdhs bcjhsdbcjhdsbcjhbsdjchbdjshc bdsjh bchj sbdcjsdbcjhsbdhjcb sdjhbcjdhsbcjs hsdb chjsdbcjhsdb',
    descriptionAuthor: 'Julie Delplace',
    active: true
  });
});

User.find({}).remove(function() {
  User.create({
    _id: '650467e8bfd12eac154ad6af',
    provider: 'local',
    name: 'Test User',
    email: 'test@test.com',
    money: 100,
    password: 'test',
    active: true
  }, {
    provider: 'local',
    name: 'Test 1 User',
    email: 'test1@test.com',
    password: 'test',
    active: true
  },{
    provider: 'local',
    name: 'Test 2 User',
    email: 'test2@test.com',
    password: 'test',
    active: true
  },{
    provider: 'local',
    name: 'Test 3 User',
    email: 'test3@test.com',
    password: 'test',
    active: true
  },{
    provider: 'local',
    name: 'Test 4 User',
    email: 'test4@test.com',
    password: 'test',
    active: true
  },{
    provider: 'local',
    name: 'Test 5 User',
    email: 'test5@test.com',
    password: 'test',
    active: true
  },{
    provider: 'local',
    name: 'Test 6 User',
    email: 'test6@test.com',
    password: 'test',
    active: true
  },{
    provider: 'local',
    name: 'Test 7 User',
    email: 'test7@test.com',
    password: 'test',
    active: true
  },{
    provider: 'local',
    name: 'Test 8 User',
    email: 'test8@test.com',
    password: 'test',
    active: true
  },{
    provider: 'local',
    name: 'Test 9 User',
    email: 'test9@test.com',
    password: 'test',
    active: true
  },{
    provider: 'local',
    name: 'Test 10 User',
    email: 'test10@test.com',
    password: 'test',
    active: true
  },{
    provider: 'local',
    name: 'Test 11 User',
    email: 'test11@test.com',
    password: 'test',
    active: true
  },{
    provider: 'local',
    name: 'Test 12 User',
    email: 'test12@test.com',
    password: 'test',
    active: true
  },{
    provider: 'local',
    name: 'Test 13 User',
    email: 'test13@test.com',
    password: 'test',
    active: true
  },{
    provider: 'local',
    name: 'Test 14 User',
    email: 'test14@test.com',
    password: 'test',
    active: true
  },{
    provider: 'local',
    name: 'Test 15 User',
    email: 'test15@test.com',
    password: 'test',
    active: true
  },{
    provider: 'local',
    name: 'Test 16 User',
    email: 'test16@test.com',
    password: 'test',
    active: true
  },{
    provider: 'local',
    name: 'Test 17 User',
    email: 'test17@test.com',
    password: 'test',
    active: true
  },{
    provider: 'local',
    name: 'Test 18 User',
    email: 'test18@test.com',
    password: 'test',
    active: true
  },{
    provider: 'local',
    name: 'Test 19 User',
    email: 'test19@test.com',
    password: 'test',
    active: true
  },{
    provider: 'local',
    name: 'Test 20 User',
    email: 'test20@test.com',
    password: 'test',
    active: true
  },{     
    provider: 'local',
    role: 'admin',
    name: 'Aadmini',
    email: 'admin@admin.com',
    password: 'admin',
    active: true
  }, {
    _id: '750467e8bfd12eac154ad6af',
    provider: 'local',
    role: 'admin',
    name: 'Admin',
    email: 'admin@admin.com',
    password: 'admin',
    active: true
  }, function() {
      console.log('finished populating users');
    }
  );
});
